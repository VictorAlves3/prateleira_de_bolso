import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-nav-bar-mobile',
  templateUrl: './nav-bar-mobile.component.html',
  styleUrls: ['./nav-bar-mobile.component.css']
})
export class NavBarMobileComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit() {
  }

}
