import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ColecaoService } from '../services/colecao.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-editar-colecao',
  templateUrl: './editar-colecao.component.html',
  styleUrls: ['./editar-colecao.component.css']
})
export class EditarColecaoComponent implements OnInit {

  id;
  collec = {
    _id: "",
    nome: "",
    descricao: "",
    id_usuario: "",
    img: ""
  };
  constructor(private location: Location, private route: ActivatedRoute, public collections: ColecaoService) { }

  goBack() {
    this.location.back();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params["id"];
      this.collec._id = this.id;
      this.collections.getCollectionById(this.id)
        .subscribe(colecao => {
          this.collec._id = colecao[0]._id;
          this.collec.descricao = colecao[0].descricao;
          this.collec.id_usuario = colecao[0].id_usuario;
          this.collec.img = colecao[0].img;
          this.collec.nome = colecao[0].nome;
        });
    });
  }

  atualizarColecao() {
    this.collections.updateCollection(this.collec)
      .subscribe(collection => {
        console.log(collection);
      });
    this.location.back();
  }

  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var url = readerEvt.target.result;
    this.collec.img = url;
  }


}
