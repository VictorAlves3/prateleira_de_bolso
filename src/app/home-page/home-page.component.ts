import { Component, OnInit, ElementRef } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { ColecaoService } from '../services/colecao.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  idUsuario = "";
  collec = {
    nome: "",
    descricao: "",
    id_usuario: this.idUsuario,
    img: "../../assets/default-icons/photo.jpg"
  }

  imagem = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQEW6w2zXrXhhsMqpp4qH1FMcZ6o64gwsb80L_VGVlhtNEux1odQw";
  constructor(public collections: ColecaoService, public auth: AuthService) {
    this.idUsuario = this.auth.id;
    this.collec.id_usuario = this.idUsuario;
  }

  ngOnInit() {
    this.collections.colecoes = [];
    this.collections.getColecaoByUserId(this.auth.id)
      .subscribe(x => {
        this.collections.colecoes = x;
      });
  }
  addCollection() {
    this.collections.addColecao(this.collec)
      .subscribe(res => {
        console.log(res);
        this.collections.colecoes.push(res);
      });
    this.limpar();

  }

  limpar() {
    this.collec.descricao = "";
    this.collec.nome = "";
    this.collec.img = "../../assets/default-icons/photo.jpg";
  }
  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var url = readerEvt.target.result;
    this.collec.img = url;
  }

}
