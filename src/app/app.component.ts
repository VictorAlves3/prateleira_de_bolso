import { Component } from '@angular/core';
import { UsuarioService } from './services/usuario.service';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  usuarios;
  isAuth;
  constructor(private auth: AuthService) { }
  ngOnInit() {
    var data = Date()
    console.log(data);
    console.log(data)
  }
  ngDoCheck() {
    this.isAuth = this.auth.isAuth;
  }
  title = 'app';
}
