import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterializeModule } from 'angular2-materialize';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { NavBarMobileComponent } from './nav-bar-mobile/nav-bar-mobile.component';
import { NavBarDesktopComponent } from './nav-bar-desktop/nav-bar-desktop.component';
import { LoginComponent } from './login/login.component';
import { ListItemComponent } from './list-item/list-item.component';
import { ColecaoComponent } from './colecao/colecao.component';
import { ColecoesComponent } from './colecoes/colecoes.component';
import { VisualizacaoColecaoComponent } from './visualizacao-colecao/visualizacao-colecao.component';
import { RoutingModule } from './app.routing';
import { FooterComponent } from './footer/footer.component';
import { VisualizacaoItemComponent } from './visualizacao-item/visualizacao-item.component';
import { PerfilConfigComponent } from './perfil-config/perfil-config.component';
import { UsuarioService } from './services/usuario.service';
import { ColecaoService } from './services/colecao.service';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guardas/app.auth.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CadastrarComponent } from './cadastrar/cadastrar.component';
import { RecuperarSenhaComponent } from './recuperar-senha/recuperar-senha.component';
import { ModalDeleteComponent } from './modal-delete/modal-delete.component';
import { AjudaComponent } from './ajuda/ajuda.component';
import { EditarColecaoComponent } from './editar-colecao/editar-colecao.component';
import { ServersService } from './servers.service';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    SideNavComponent,
    NavBarMobileComponent,
    NavBarDesktopComponent,
    LoginComponent,
    ListItemComponent,
    ColecaoComponent,
    ColecoesComponent,
    VisualizacaoColecaoComponent,
    FooterComponent,
    VisualizacaoItemComponent,
    PerfilConfigComponent,
    PageNotFoundComponent,
    CadastrarComponent,
    RecuperarSenhaComponent,
    ModalDeleteComponent,
    AjudaComponent,
    EditarColecaoComponent
  ],
  imports: [
    BrowserModule,
    MaterializeModule,
    RoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    UsuarioService,
    ColecaoService,
    AuthService,
    AuthGuard,
    ServersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
