import { Component, OnInit, HostListener } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(public auth: AuthService, private router: Router) { }
  @HostListener("window:keyup", ["$event"])
  scroll(e: KeyboardEvent) {
    if (e.key == "Enter") {
      this.logar();
    }
  }

  invalid = false;

  validarUsuario(usuario): boolean {
    if (usuario.length >= 4 && usuario.length < 50) {
      return true;
    }
    return false;
  }

  validarSenha(senha): boolean {
    if (senha.length >= 4 && senha.length < 20) {
      return true;
    }
    return false;
  }

  validarForm() {
    if (this.validarSenha(this.user.pass) && this.validarUsuario(this.user.user)) {
      return true;
    }
    console.log(false);
    return false;
  }

  user = {
    user: "user",
    pass: "pass"
  }
  ngOnInit() {
  }
  logar() {
    if (this.validarForm()) {
      let x = this.auth.autenticar(this.user)
        .subscribe(x => {
          if (typeof (x) == "string" && x.length >= 20) {
            this.auth.isAuth = true;
            this.auth.id = x;
            this.router.navigate(["/"]);
            this.invalid = false;
          }
        });
    }
    if (this.auth.isAuth) {
      this.invalid = false;
    }
    else {
      this.invalid = true;
    }
  }

}
