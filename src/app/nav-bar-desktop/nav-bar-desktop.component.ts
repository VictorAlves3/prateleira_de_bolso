import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-nav-bar-desktop',
  templateUrl: './nav-bar-desktop.component.html',
  styleUrls: ['./nav-bar-desktop.component.css']
})
export class NavBarDesktopComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit() {
  }

}
