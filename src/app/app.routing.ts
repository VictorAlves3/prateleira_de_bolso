import { Routes, RouterModule } from '@angular/router'
import { HomePageComponent } from './home-page/home-page.component';
import { VisualizacaoColecaoComponent } from './visualizacao-colecao/visualizacao-colecao.component';
import { VisualizacaoItemComponent } from './visualizacao-item/visualizacao-item.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './guardas/app.auth.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CadastrarComponent } from './cadastrar/cadastrar.component';
import { RecuperarSenhaComponent } from './recuperar-senha/recuperar-senha.component';
import { AjudaComponent } from './ajuda/ajuda.component';
import { EditarColecaoComponent } from './editar-colecao/editar-colecao.component';
const routes: Routes = [
    {
        path: "",
        component: HomePageComponent,
        canActivate: [
            AuthGuard
        ]
    },
    {
        path: "colecao/:id",
        component: VisualizacaoColecaoComponent,
        canActivate: [
            AuthGuard
        ]
    },
    {
        path: "editcolecao/:id",
        component: EditarColecaoComponent,
        canActivate: [
            AuthGuard        
        ]
    },
    {
        path: "item/:id",
        component: VisualizacaoItemComponent,
        canActivate: [
            AuthGuard
        ]
    },
    {
        path: "cadastrar",
        component: CadastrarComponent
    },
    {
        path: "recuperarsenha",
        component: RecuperarSenhaComponent
    },
    {
        path: "ajuda",
        component: AjudaComponent
    },
    {
        path: "login",
        component: LoginComponent
    },
    {
        path: "404",
        component: PageNotFoundComponent
    },
    {
        path: "**",
        component: PageNotFoundComponent
    },
    {
        path: "*path",
        component: PageNotFoundComponent
    }
]

export const RoutingModule = RouterModule.forRoot(routes);