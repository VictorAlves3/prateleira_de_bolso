import { EventEmitter, Component, OnInit, Input, Output } from '@angular/core';


@Component({
  selector: 'app-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.css']
})
export class ModalDeleteComponent implements OnInit {
  @Input() titulo;
  @Input() pergunta;
  @Output() excluir = new EventEmitter;

  constructor() { }
  apagar() {
    this.excluir.emit("botão apagar clicado");
  }

  ngOnInit() {
  }

}
