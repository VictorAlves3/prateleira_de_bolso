import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient } from 'selenium-webdriver/http';
import { ColecaoService } from '../services/colecao.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  @Input() item;
  @Input() link;
  constructor(private location: Location, public collections: ColecaoService) { }

  ngOnInit() {
  }
  deleteItem() {
    this.collections.deleteItem(this.item._id)
      .subscribe(x => {
        console.log(x);
        this.collections.spliceItemById(this.item._id);
      });
  }
}
