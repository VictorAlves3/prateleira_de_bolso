import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.css']
})
export class CadastrarComponent implements OnInit {
  form = {
    nome: "",
    email: "",
    senha: "",
    repetirSenha: ""
  }
  constructor(public users: UsuarioService, public router: Router) { }

  ngOnInit() {
  }
  onSubmit() {
    let usuario = {
      user: this.form.nome,
      pass: this.form.senha,
      email: this.form.email
    }
    this.users.addUsuario(usuario)
      .subscribe(x => console.log(x));
    this.router.navigate(["/login"]);
  }

}
