import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizacaoColecaoComponent } from './visualizacao-colecao.component';

describe('VisualizacaoColecaoComponent', () => {
  let component: VisualizacaoColecaoComponent;
  let fixture: ComponentFixture<VisualizacaoColecaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizacaoColecaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacaoColecaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
