import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { ColecaoService } from '../services/colecao.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-visualizacao-colecao',
  templateUrl: './visualizacao-colecao.component.html',
  styleUrls: ['./visualizacao-colecao.component.css']
})
export class VisualizacaoColecaoComponent implements OnInit {
  id;
  nomeColecao;
  item = {
    nome: "",
    descricao: "",
    id_colecao: this.id,
    img: "../../assets/default-icons/photo.jpg"
  }

  constructor(public route: ActivatedRoute, public location: Location, public collections: ColecaoService) {

  }
  ngOnInit() {
    this.route.params.subscribe(param => {
      this.id = param["id"];
      this.item.id_colecao = this.id;
      this.collections.itens = [];
      this.collections.getItemsByCollectionId(this.id)
        .subscribe(res => {
          this.collections.itens = res;
        });
    });
    this.nomeColecao = this.collections.findCollectionNameById(this.id);

  }
  apagarColecao() {
    this.collections.deleteColecao(this.id)
      .subscribe(del => {
        this.collections.itens = [];
        for (let i of this.collections.colecoes) {
          if (i._id == this.id) {
            this.collections.colecoes.splice(this.collections.colecoes.indexOf(i), 1);
          }
        }
        this.location.back();
      });
  }
  addItem() {
    this.collections.addItem(this.item)
      .subscribe(item => {
        this.collections.itens.push(item.ops[0]);
      })
    this.limpar();
  }

  limpar() {
    this.item.descricao = "";
    this.item.nome = "";
    this.item.img = "../../assets/default-icons/photo.jpg";
  }

  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var url = readerEvt.target.result;
    this.item.img = url;
  }
}
