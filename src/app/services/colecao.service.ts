import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import "rxjs/add/operator/map";
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { ServersService } from '../servers.service';

@Injectable()
export class ColecaoService {

  constructor(private servers: ServersService, private http: HttpClient, private auth: AuthService) {


  }

  colecoes = [];
  itens = [];
  ngDoCheck() {

  }
  getCollectionById(id: string): Observable<any> {
    return this.http.get(this.servers.kinghostApi + "api/collections/" + id)
  }
  getColecaoByUserId(userId: string): Observable<any> {
    if (typeof (userId) == "string" && userId.length == 24) {
      {
        return this.http.get(this.servers.kinghostApi + "api/collections/by/user/" + userId)
      }
    }
  }
  updateCollection(colecao): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token',
        "Access-Control-Allow-Origin": "*"
      })
    };
    let id = colecao._id;
    return this.http.put(this.servers.kinghostApi + "api/collections/" + id, colecao, httpOptions);
  }
  addColecao(colecao): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token',
        "Access-Control-Allow-Origin": "*"
      })
    };
    return this.http.post(this.servers.kinghostApi + "api/collections/", colecao, httpOptions)

  }

  deleteColecao(id) {
    console.log(id);
    return this.http.delete(this.servers.kinghostApi + "api/collections/" + id);
  }

  getItemsByCollectionId(collectionId): Observable<any> {
    if (typeof (collectionId) == "string" && collectionId.length == 24) {
      return this.http.get(this.servers.kinghostApi + "api/items/by/collection/" + collectionId);
    }
  }
  addItem(item): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token',
        "Access-Control-Allow-Origin": "*"
      })
    };
    return this.http.post(this.servers.kinghostApi + "api/items/", item, httpOptions);
  }
  getItembyId(id): Observable<any> {
    return this.http.get(this.servers.kinghostApi + "api/items/" + id);
  }
  deleteItem(id): Observable<any> {
    return this.http.delete(this.servers.kinghostApi + "api/items/" + id);
  }
  updateItem(id, item): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token',
        "Access-Control-Allow-Origin": "*"
      })
    };
    return this.http.put(this.servers.kinghostApi + "api/items/" + id, item, httpOptions);
  }

  findCollectionNameById(collectionId) {
    let nome;
    for (let collection of this.colecoes) {
      if (collection._id == collectionId) {
        nome = collection.nome;
      }
    }
    return nome;
  }
  spliceItemById(id) {
    for (let i of this.itens) {
      if (i._id == id) {
        this.itens.splice(this.itens.indexOf(i), 1);
      }
    }
  }

}
