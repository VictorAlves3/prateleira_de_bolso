import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ServersService } from '../servers.service';
@Injectable()
export class AuthService {
  id = "";
  isAuth: boolean = false;
  constructor(private http: HttpClient, private router: Router, private servers: ServersService) { }
  autenticar(user): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token',
        "Access-Control-Allow-Origin": "*"
      })
    };
    return this.http.post(this.servers.kinghostApi + "api/users/auth", user, httpOptions);
  }
  logOut() {
    this.isAuth = false;
    this.router.navigate(["/login"]);
  }
}
