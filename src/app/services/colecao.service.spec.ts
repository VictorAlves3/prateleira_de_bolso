import { TestBed, inject } from '@angular/core/testing';

import { ColecaoService } from './colecao.service';

describe('ColecaoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ColecaoService]
    });
  });

  it('should be created', inject([ColecaoService], (service: ColecaoService) => {
    expect(service).toBeTruthy();
  }));
});
