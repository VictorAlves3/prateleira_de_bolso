import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/map';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { headersToString } from 'selenium-webdriver/http';
import { Observable } from "rxjs";
import { ServersService } from '../servers.service';
@Injectable()
export class UsuarioService {
  usuarios = [];
  constructor(private http: HttpClient, private servers: ServersService) {
    this.getAllUsers();
  }

  getAllUsers(): Observable<any> {
    return this.http.get(this.servers.kinghostApi + "api/users/5adde6f5aaf1a21dbca1d482");
  }
  getUsuarioById(idUsuario) {

  }
  updateUsuario(usuario) {

  }
  addUsuario(usuario): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token',
        "Access-Control-Allow-Origin": "*"
      })
    };
    return this.http.post(this.servers.kinghostApi + "api/users/", usuario, httpOptions);
  }
  deleteUsuario(usuario) {

  }
  validarNome(nome) {

  }
  validarSenha(senha) {

  }
  validarEmail(email) {

  }
}
