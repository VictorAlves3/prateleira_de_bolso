import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ColecaoService } from '../services/colecao.service';

@Component({
  selector: 'app-visualizacao-item',
  templateUrl: './visualizacao-item.component.html',
  styleUrls: ['./visualizacao-item.component.css']
})
export class VisualizacaoItemComponent implements OnInit {
  id;
  item = {
    _id: "",
    nome: "",
    descricao: "",
    id_colecao: '',
    img: ''

  };
  constructor(private location: Location, private route: ActivatedRoute, public collections: ColecaoService) { }

  goBack() {
    this.location.back();
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params["id"];
    });
    this.collections.getItembyId(this.id)
      .subscribe(item => {
        this.item.nome = item[0].nome;
        this.item.descricao = item[0].descricao;
        this.item._id = item[0]._id;
        this.item.id_colecao = item[0].id_colecao;
        this.item.img = item[0].img;
      });
  }
  atualizarItem() {
    this.collections.updateItem(this.id, this.item)
      .subscribe(item => {
        console.log(item);
        this.location.back();
      });
  }
  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var url = readerEvt.target.result;
    this.item.img = url;
  }

}
